import { PageRoutings } from "../enums/page-routings.enum";
import { StepperButtons, StepperInputLabels, StepperStepsLabels } from "../enums/stepper-labels.enum";
import { Warnings } from "../enums/warnings.enum";
import BasePage from "./BasePage";

export default class StepperPage extends BasePage {
    
    protected stepperPageNavLink: string;
    protected inputWarningText: string;
    stepperComponentSelector: string;

    constructor() {
        super();
        this.stepperComponentSelector = 'app-stepper';
        this.stepperPageNavLink = '#stepper-view-link';
        this.inputWarningText = 'form .text-danger';
    }

    goToStepperPage() {
        cy.get(this.stepperPageNavLink).click();
    }

    assertThatStepperPageIsOpened() {
        this.checkIfUrlContains(PageRoutings.Stepper);
        cy.get(this.stepperComponentSelector).should('exist');
    }

    generateStepperInputSelector(inputLabel: StepperInputLabels) {
        return cy.get(this.stepperComponentSelector).find(`mat-label:contains("${inputLabel}")`).closest('div').find('input')
    }

    fillStepperInput(inputLabel: StepperInputLabels, value: string) {
        this.generateStepperInputSelector(inputLabel).type(value);
    }

    checkStepperInputValue(inputLabel: StepperInputLabels, value: string) {
        this.generateStepperInputSelector(inputLabel).should('have.value', value);
    }

    clearStepperInput(inputLabel: StepperInputLabels) {
        this.generateStepperInputSelector(inputLabel).clear().blur();
    }

    navigateThroughStepper(buttonLabel: StepperButtons) {
        cy.get(`div[role="tabpanel"] button:contains("${buttonLabel}"):visible`).click();
    }

    assertStepperCompletion(label: string, value: string) {
        cy.get(this.stepperComponentSelector).find('p:contains("You are now done!")').then(($el) => {
            cy.wrap($el).siblings('p').contains(`${label}: ${value}`);
        });
    }

    assertThatCurrentStepIsActive(stepLabel: StepperStepsLabels) {
        cy.get('.mat-step-text-label').then(($step) => {
            cy.wrap($step).contains(stepLabel).closest('mat-step-header').should('have.attr', 'aria-selected', 'true');
        })
    }

    assertThatInputWarningIsVisible(warningText: Warnings) {
        cy.get(this.inputWarningText).should('be.visible').and('contain', warningText);
    }
    
}