import { FormLabels } from "../enums/form-values.enum";
import { PageRoutings } from "../enums/page-routings.enum";
import BasePage from "./BasePage";
export default class FormPage extends BasePage {

    protected formComponentSelector: string;
    formPageNavLink: string;
    nameInput: string;
    alterEgoInput: string;
    heroPowerSelect: string;
    submitButton: string;
    newHeroButton: string;

    constructor() {
        super();
        this.formComponentSelector = 'app-form';
        this.formPageNavLink = '#form-view-link';
        this.nameInput = '#name';
        this.alterEgoInput = '#alterEgo';
        this.heroPowerSelect = '#power';
        this.submitButton = 'button.btn-success[type="submit"]';
        this.newHeroButton = 'button.btn-default:contains("New Hero")';
    }

    goToFormPage() {
        cy.get(this.formPageNavLink).click();
    }

    assertThatFormPageIsOpened() {
        this.checkIfUrlContains(PageRoutings.Form);
        cy.get(this.formComponentSelector).should('exist');
    }

    shouldSubmitButtonBeDisabled(shouldBeDisabled: boolean) {
        shouldBeDisabled ? cy.get(this.submitButton).should('be.disabled') : cy.get(this.submitButton).should('not.be.disabled');
    }

    verifySubmittedForm(name: string, alterEgo: string, power: string) {
        cy.get(`${this.formComponentSelector}`).find('.results').should('not.have.attr', 'hidden').then(($container) => {
            cy.wrap($container).contains('You submitted the following:');
            this.verifyRowValue(FormLabels.Name, name);
            this.verifyRowValue(FormLabels.AlterEgo, alterEgo);
            this.verifyRowValue('Power', power);
        });
    }

    private verifyRowValue(label: string, value: string) {
        cy.get(`div.row:contains("${label}")`).children().eq(1).should('have.text', value);
    }
}