import { PageRoutings } from "../enums/page-routings.enum";

export default class BasePage {

    protected alertSelector: string;
    angularOnTwitterIcon: string;
    angularOnYoutubeIcon: string;

    constructor() {
        this.alertSelector = '.alert';
        this.angularOnTwitterIcon = '#angular-on-twitter-svg';
        this.angularOnYoutubeIcon = '#angular-on-youtube-svg';
    }

    visitPage(pageRouting?: PageRoutings) {
        pageRouting ? cy.visit(`${pageRouting}`) : cy.visit('/');
    }

    checkIfUrlContains(value: string, shouldContain: boolean = true) {
        cy.url().should(shouldContain ? 'include' : 'not.include', value);
    }

    // I'm aware that this kind of functions might not be a good practice but I decided to go fully POM aproach to have a self-commenting code
    // I thought also to add this method as a Cypress.command but I'd rather to have everything in one place.
    verifyInputValue(inputSelector: string, expectedValue: string | number) {
        cy.get(inputSelector).should('have.value', expectedValue);
    }

    clearAndCheckInput(inputSelector: string) {
        cy.get(inputSelector).clear().blur().should('have.text', '');
    }

    clearAndType(inputSelector: string, value: string) {
        this.clearAndCheckInput(inputSelector);
        cy.get(inputSelector).type(value).blur();
    }

    isAlertVisible(isVisible: boolean, alertText?: string) {
        if (isVisible) {
            cy.get(`${this.alertSelector}:not([hidden])`).should('be.visible').then(($el) => {
                alertText ? expect($el.text().trim()).to.eq(alertText) : undefined
            })
        } else {
            cy.get(this.alertSelector).should('not.be.visible');
        }
    }
}