import BasePage from "./BasePage";

export default class WelcomePage extends BasePage {
    
    protected contentComponentSelector: string;
    protected welcomePageNavLink: string;

    constructor() {
        super();
        this.contentComponentSelector = 'app-home';
        this.welcomePageNavLink = '#main-view-link';
    }

    goToWelcomePage() {
        cy.get(this.welcomePageNavLink).click();
    }

    assertThatWelcomePageIsOpened() {
        cy.url().should('eq', Cypress.config().baseUrl);
        cy.get(this.contentComponentSelector).should('exist');
    }
}