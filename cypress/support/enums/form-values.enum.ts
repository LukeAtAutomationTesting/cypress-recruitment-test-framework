export enum FormLabels {
    Name = 'Name',
    AlterEgo = 'Alter Ego',
    HeroPower = 'Hero Power',
}

export enum FormDefaultInputValues {
    Name = 'Dr IQ',
    AlterEgo = 'Chuck Overstreet',
    HeroPower = 'Really Smart'
}

export enum HeroPowerSelectOptions {
    ReallySmart = 'Really Smart',
    SuperFlexible = 'Super Flexible',
    SuperHot = 'Super Hot',
    WeatherChanger = 'Weather Changer'
}