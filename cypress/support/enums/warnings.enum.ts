export enum Warnings {
    MaxCharInput = 'The maximum length for this field is 20 characters.',
    RequiredField = 'This field is required.'
}