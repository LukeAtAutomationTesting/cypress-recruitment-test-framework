export enum StepperInputLabels {
    Name = 'Name',
    Address = 'Address'
}

export enum StepperStepsLabels {
    FirstStep = 'Fill out your name',
    SecondStep = 'Fill out your address',
    ThirdStep = 'Done'
}

export enum StepperButtons {
    Next = 'Next',
    Back = 'Back',
    Reset = 'Reset'
}