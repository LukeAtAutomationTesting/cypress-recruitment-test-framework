/// <reference types="cypress" />

import FormPage from "../../support/Pages/FormPage";
import WelcomePage from "../../support/Pages/WelcomePage";
import { FormDefaultInputValues, HeroPowerSelectOptions } from "../../support/enums/form-values.enum";
import { PageRoutings } from "../../support/enums/page-routings.enum";

describe('Tests on Form Page', () => {
    const welcomePage = new WelcomePage;
    const formPage = new FormPage;

    beforeEach(() => {
        welcomePage.visitPage(PageRoutings.Form);
        welcomePage.checkIfUrlContains(PageRoutings.Form, true);
    });

    it('Check default values inside the form inputs', () => {
        formPage.verifyInputValue(formPage.nameInput, FormDefaultInputValues.Name);
        formPage.verifyInputValue(formPage.alterEgoInput, FormDefaultInputValues.AlterEgo);
        formPage.verifyInputValue(formPage.heroPowerSelect, FormDefaultInputValues.HeroPower);
    });

    it('Clear the "Name" input and check the submit possibility', () => {
        formPage.clearAndCheckInput(formPage.nameInput);
        formPage.isAlertVisible(true, 'Name is required');
        formPage.shouldSubmitButtonBeDisabled(true);
    });

    it('Clear the "Alter Ego" input and check the submit possibility', () => {
        formPage.clearAndCheckInput(formPage.alterEgoInput);
        formPage.isAlertVisible(false);
        formPage.shouldSubmitButtonBeDisabled(false);
    });

    it('Submit new hero with the defualt values', () => {
        cy.get(formPage.submitButton).click();
        formPage.assertThatFormPageIsOpened();
        formPage.verifySubmittedForm(FormDefaultInputValues.Name, FormDefaultInputValues.AlterEgo, FormDefaultInputValues.HeroPower);

    });

    it('Submit new hero by replacing defualt values', () => {
        cy.fixture('testData.json').then((testData) => {
            formPage.clearAndType(formPage.nameInput, testData.Name);
            formPage.clearAndType(formPage.alterEgoInput, testData.AlterEgo);
            cy.get(formPage.heroPowerSelect).select(testData.Power);
            cy.get(formPage.submitButton).click();
            formPage.assertThatFormPageIsOpened();
            formPage.verifySubmittedForm(testData.Name, testData.AlterEgo, testData.Power);
        });
    });

    it('Submitting hero using "New Hero" button to clear inputs', () => {
        const newHeroName = 'New Hero Name';
        const newHeroAlterEgo = 'New Hero Alter Ego';

        cy.get(formPage.newHeroButton).click();
        formPage.verifyInputValue(formPage.nameInput, '');
        formPage.verifyInputValue(formPage.alterEgoInput, '');
        formPage.verifyInputValue(formPage.heroPowerSelect, null);

        cy.get(formPage.nameInput).type(newHeroName);
        cy.get(formPage.alterEgoInput).type(newHeroAlterEgo);
        cy.get(formPage.heroPowerSelect).select(HeroPowerSelectOptions.WeatherChanger);

        cy.get(formPage.submitButton).click();
        formPage.assertThatFormPageIsOpened();
        formPage.verifySubmittedForm(newHeroName, newHeroAlterEgo, HeroPowerSelectOptions.WeatherChanger);
    });

});
