/// <reference types="cypress" />

import WelcomePage from "../../support/Pages/WelcomePage";
import { PageRoutings } from "../../support/enums/page-routings.enum";

describe('Tests on Welcome Page (Homepage)', () => {
   const welcomePage = new WelcomePage;

   beforeEach(() => {
      welcomePage.visitPage();
      welcomePage.checkIfUrlContains(PageRoutings.Form, false);
      welcomePage.checkIfUrlContains(PageRoutings.Stepper, false);
   });

   it('Verifying elements on the Welcome Page', () => {
      cy.get('svg#rocket')
         .should('be.visible')
         .closest('.card')
         .should('contain.text', 'Recruitment app is running!');
   });
});

