/// <reference types="cypress" />

import FormPage from "../../support/Pages/FormPage";
import StepperPage from "../../support/Pages/StepperPage";
import WelcomePage from "../../support/Pages/WelcomePage";
import { PageRoutings } from "../../support/enums/page-routings.enum";

describe('Navigation tests from the "Welcome" Page', () => {
   const welcomePage = new WelcomePage;
   const formPage = new FormPage;
   const stepperPage = new StepperPage;

   beforeEach(() => {
      welcomePage.visitPage();
      welcomePage.assertThatWelcomePageIsOpened();
      welcomePage.checkIfUrlContains(PageRoutings.Form, false);
      welcomePage.checkIfUrlContains(PageRoutings.Stepper, false);
   });

   it('Switching page to "Form" from the nav bar', () => {
      formPage.goToFormPage();
      formPage.assertThatFormPageIsOpened();
   });

   it('Switching page to "Stepper" from the nav bar', () => {
      stepperPage.goToStepperPage();
      stepperPage.assertThatStepperPageIsOpened();
   });

   it('Switching page to Youtube', () => {
      cy.get(welcomePage.angularOnYoutubeIcon).then(($el) => {
         cy.wrap($el).closest('a[title="YouTube"]').invoke('removeAttr', 'target').click();
         welcomePage.checkIfUrlContains('youtube.com', true);
      });
   });
   
   it('Switching page to Twitter', () => { // TODO: flaky
      cy.get(welcomePage.angularOnTwitterIcon).then(($el) => {
         cy.wrap($el).closest('a[title="Twitter"]').invoke('removeAttr', 'target').click();
         welcomePage.checkIfUrlContains('twitter.com', true);
      });
   });
});