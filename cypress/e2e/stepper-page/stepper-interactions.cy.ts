/// <reference types="cypress" />

import StepperPage from "../../support/Pages/StepperPage";
import WelcomePage from "../../support/Pages/WelcomePage";
import { PageRoutings } from "../../support/enums/page-routings.enum";
import { StepperButtons, StepperInputLabels, StepperStepsLabels } from "../../support/enums/stepper-labels.enum";
import { Warnings } from "../../support/enums/warnings.enum";

describe('Tests on Form Page', () => {
    const welcomePage = new WelcomePage;
    const stepperPage = new StepperPage;

    beforeEach(() => {
        welcomePage.visitPage(PageRoutings.Stepper);
        welcomePage.checkIfUrlContains(PageRoutings.Stepper, true);
    });

    it('Completing the entire stepper process and reset', () => {
        cy.fixture('testData.json').then((testData) => {
            stepperPage.fillStepperInput(StepperInputLabels.Name, testData.Name);
            stepperPage.navigateThroughStepper(StepperButtons.Next);
            stepperPage.assertThatCurrentStepIsActive(StepperStepsLabels.SecondStep);
            stepperPage.fillStepperInput(StepperInputLabels.Address, testData.Address);
            stepperPage.navigateThroughStepper(StepperButtons.Next);
            stepperPage.assertThatCurrentStepIsActive(StepperStepsLabels.ThirdStep);
            stepperPage.assertStepperCompletion(StepperInputLabels.Name, testData.Name);
            stepperPage.assertStepperCompletion(StepperInputLabels.Address, testData.Address);

            stepperPage.navigateThroughStepper(StepperButtons.Reset);
            stepperPage.assertThatCurrentStepIsActive(StepperStepsLabels.FirstStep);
            stepperPage.checkStepperInputValue(StepperInputLabels.Name, '');
        });
    });

    it('Going back the stepper after first step', () => {
        cy.fixture('testData.json').then((testData) => {
            stepperPage.fillStepperInput(StepperInputLabels.Name, testData.Name);
            stepperPage.navigateThroughStepper(StepperButtons.Next);
            stepperPage.assertThatCurrentStepIsActive(StepperStepsLabels.SecondStep);
            stepperPage.navigateThroughStepper(StepperButtons.Back);
            stepperPage.assertThatCurrentStepIsActive(StepperStepsLabels.FirstStep);
            stepperPage.checkStepperInputValue(StepperInputLabels.Name, testData.Name);
        });
    });

    it('Validate maximum characters inside the stepper inputs', () => {
        stepperPage.fillStepperInput(StepperInputLabels.Name, 'This is an example!!!');
        stepperPage.assertThatInputWarningIsVisible(Warnings.MaxCharInput);
    });

    it('Validate maximum characters inside the stepper inputs', () => {
        stepperPage.fillStepperInput(StepperInputLabels.Name, 'This is an example');
        stepperPage.clearStepperInput(StepperInputLabels.Name);
        stepperPage.assertThatInputWarningIsVisible(Warnings.RequiredField);
    });

});
