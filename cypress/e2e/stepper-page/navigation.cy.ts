/// <reference types="cypress" />

import FormPage from "../../support/Pages/FormPage";
import StepperPage from "../../support/Pages/StepperPage";
import WelcomePage from "../../support/Pages/WelcomePage";
import { PageRoutings } from "../../support/enums/page-routings.enum";

describe('Navigation tests from the "Stepper" Page', () => {
    const welcomePage = new WelcomePage;
    const formPage = new FormPage;
    const stepperPage = new StepperPage;

    beforeEach(() => {
        welcomePage.visitPage(PageRoutings.Stepper);
        stepperPage.assertThatStepperPageIsOpened();
    });

    it('Incorrect URL directs to the "Welcome" Page (homepage)', () => {
        stepperPage.visitPage(PageRoutings.IncorrectStepperRouting);
        welcomePage.assertThatWelcomePageIsOpened();
    });

    it('Switching page to "Welcome" from the nav bar', () => {
        welcomePage.goToWelcomePage();
        welcomePage.assertThatWelcomePageIsOpened();
        welcomePage.checkIfUrlContains(PageRoutings.Form, false);
        welcomePage.checkIfUrlContains(PageRoutings.Stepper, false);
    });

    it('Switching page to "Form" from the nav bar', () => {
        formPage.goToFormPage();
        formPage.assertThatFormPageIsOpened();
    });
});