# Cypress Recruitment Test Framework

This repository contains a testing framework for conducting recruitment tests using Cypress.

## How to Run Tests

### Using Cypress CLI

To run all tests using the Cypress CLI tool, follow these steps:

1. Ensure you have Cypress installed globally on your system. If not, install it using:  
`npm install -g cypress`

2. Navigate to the repository's directory on your local system:  
`cd /path/to/repository`

3. Run Cypress using the following command:  
`cypress run`

Cypress will run all tests automatically in the background and display the results in the console.

### Using the `npx cypress open` Command

To run tests using Cypress via the `npx cypress open` command, follow these steps:

1. Ensure you have Node.js installed on your system.

2. Navigate to the repository's directory on your local system:  
`cd /path/to/repository`

3. Run Cypress using the following command:  
`npx cypress open`

Cypress will launch a graphical user interface that allows you to select the tests to run. Click on the desired test, and Cypress will automatically open a new browser window and execute the selected test.

---
