import { defineConfig } from "cypress";

module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://angular-qa-recruitment-app.netlify.app/',
    viewportHeight: 1080,
    viewportWidth: 1920,
    chromeWebSecurity: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
